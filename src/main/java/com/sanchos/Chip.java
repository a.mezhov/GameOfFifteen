package com.sanchos;

import javax.swing.*;
import java.awt.*;

public class Chip {

    private double x;
    private double y;
    final private double w;
    final private double h;
    final private String img;

    //Конструктор метод создающий пятнашку
    public Chip(int x , int y, double c, double d, String s){
        this.x = x;
        this.y = y;
        this.w = c;
        this.h = d;
        this.img = s;
    }

    public double getX() {return x;  }
    public double getY() {return y;  }
    public double getW() {return w;  }
    public double getH() {return h;  }

    public void setX(double x){this.x = x;}
    public void setY(double y){this.y = y;}


    // Метод графической отрисовки пятнашки
    public void drawChip(Graphics2D g) {
        g.drawImage(new ImageIcon(this.img).getImage(), (int) this.x, (int) this.y, null);//отрисовываем элемент в координатах
    }
}
