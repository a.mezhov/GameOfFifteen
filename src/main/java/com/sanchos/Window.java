package com.sanchos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class Window {
    public static void main(String[] args)  {

        JFrame startFrame = new JFrame("Game of Fifteen");// создаем окно с названием
        startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// закрытие окна при клике крестика
        startFrame.setLocation(0, 0);// имзменяем местоположение фрейма.
        Panel panel = new Panel();// создаём объект панель

        JMenuBar menuBar = new JMenuBar();
        startFrame.setJMenuBar(menuBar);

        JButton exitFromGame = new JButton("Exit");
        menuBar.add(exitFromGame);
        exitFromGame.addActionListener(e -> System.exit(0));

        startFrame.setSize(1024,768);
        startFrame.setContentPane(panel); // перенос в фрейм панели с Panel

        panel.mainTimer.start();// Запускает таймер, в результате чего он начинает отправлять события действия своим  слушателям.

        // окно видимо
        startFrame.setVisible(true);
    }
}

