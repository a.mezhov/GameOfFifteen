package com.sanchos;

import javax.swing.*;
import java.awt.*;

public class Menu {
    private Color color;
    final private double x;
    final private double y;
    final private double w;
    final private double h;
    final private String img;
    private String name;

    //Конструктор фоновой картинки
    public Menu(){
        x = 0;
        y = 0;
        w = 1024;
        h = 768;
        img = "assets/kosm.jpg";

    }

    //Конструктор кнопки в меню
    public Menu(String name, int x , int y, double w, double h, String path){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.name = name;
        this.img = path;
        color = Color.BLACK;
    }

    public void clickOnButtonInMenu(){
        if (Panel.mouseX > this.x && Panel.mouseX < this.x + this.w  && Panel.mouseY > this.y && Panel.mouseY < this.y+this.h) {// если курсор попал в кнопку
            if(Listeners.clickMenu) {
                if(this.name.equals("Выход")){
                    System.exit(0);
                }
                if(this.name.equals("Играть")){
                    Panel.state = Panel.STATES.PLAY;
                }
            }

        }
    }

    // Метод графической отрисовки фона меню
    public void drawBackgroundOfMenu(Graphics2D g) {
        g.drawImage(new ImageIcon(img).getImage(), (int) x, (int) y, null);//отрисовываем элемент в координатах
    }

    // Метод графической отрисовки кнопки в меню
    public void drawButtonInMenu(Graphics2D g) {

        g.drawImage(new ImageIcon(img).getImage(), (int) x, (int) y , null);//отрисовываем элемент в координатах
        g.setColor(color);//задаем цвет объекту Соlour
        Font font = new Font("Arial", Font.ITALIC, 60);//Создём объект класса фонт (передаем в конструктор параметры)
        g.setFont(font);//устанвливаем наш шрифт

        long length = (int) g.getFontMetrics().getStringBounds(name, g).getWidth();// длина надписи в пиксилях
        g.drawString(name, (int) (x + w / 2) - (int) (length / 2), (int) (y + (h / 3) * 2));// рисуем строчку в центре панели

    }
}