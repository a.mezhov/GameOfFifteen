package com.sanchos;

import javax.swing.*;
import java.awt.*;

// Класс создаёт поле для игры

public class Player {
    // координаты кнопки перемешивания
    final private double x;
    final private double y;
    final double w;
    final double h;
    private String img;
    final double m = 5; //промежуток между клеточек
    private String name;

    //Конструктор поля игры
    public Player(){
        x = 100;
        y = 100;
        w = 425;
        h = 425;
    }
    //Конструктор кнопки перемешивания
    public Player(String name, int x, int y, double w, double h, String path){

        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.name = name;
        this.img = path;
    }

    public double getX() { return x; }
    public double getY() { return y; }

    // если курсор попал в кнопку "запутать"
    public void clickOnShuffleButton(){
        if (Panel.mouseX > this.x && Panel.mouseX < this.x + this.w  && Panel.mouseY > this.y && Panel.mouseY < this.y + this.h) {
            if(Listeners.clickChip) {
                if(this.name.equals("Запутать")){
                    Panel.mixer = true;
                    this.img = "assets/button._d.png";
                }
            }
        }
    }

    // Метод графической отрисовки фона сетки
    public void drawGrid(Graphics2D g) {
        Color backColor = new Color(19, 203, 155);//создаем обьект класса цвет
        g.setColor(backColor);
        g.fillRect((int)x, (int)y, (int)w, (int)h);//рисуем прямоугольную область

        // Прорисовка квадратов на поле
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                Color backColor1 = new Color(196, 203, 155);
                g.setColor(backColor1);
                g.fillRect((int)(m + x + (100 + m) * j),(int)(m + y + (100 + m) * i), 100, 100);//рисуем прямоугольную область
            }
        }
    }

    // Метод графической отрисовки кнопки перемешивания
    public void drawButtonOfShuffle(Graphics2D g) {
        if (Panel.mixer) {
            img = "assets/button._d.png";
        }
        else {
            img = "assets/button._a.png";
        }
        g.drawImage(new ImageIcon(img).getImage(), (int) x, (int) y, null);//отрисовываем элемент в координатах
    }
}
