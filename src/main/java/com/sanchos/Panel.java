package com.sanchos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Panel extends JPanel implements ActionListener {

    final public static int WIDTH = 1024;
    final public static int HEIGHT = 768;

    private long timer_start = 0;// начальное время для таймера
    private long timer_end = 0;// конечное время

    ArrayList<Chip> list = new ArrayList<>(); // объявление списка для фишек
    int [] compare_x = new int[15];
    int [] compare_y = new int[15];// списки для проверочных координат

    public static int step = 0;
    final private int m = 5;// отступ от края поля игры
    final private int width = 100;// ширина игровой клетки поля
    final private int height = 100;// высота игровой клетки поля
    public static double field_x, field_y;// нач игрового поля

    public static boolean mixer = false;// разрешение на перемешивание фишек
    private boolean search = false;// разрешение на перемещение

    Menu menu = new Menu();// создаем объект фон меню
    Menu button_menu = new Menu("Играть",50,200,430,100,"assets/but1.png");
    Menu button_exit = new Menu("Выход",550,200,430,100,"assets/but1.png");
    Player field = new Player(); //создание поля игры
    Player button_shuffle = new Player("Запутать",600,10,111,100,"assets/button._a.png");

    Chip chip1 = new Chip((int) field.getX() + m, (int) field.getY() + m, width, height,"assets/g1.gif");
    Chip chip2 = new Chip((int) field.getX() + m + (width + m), (int) field.getY() + m, width, height,"assets/g2.gif");
    Chip chip3 = new Chip((int) field.getX() + m + (width + m) * 2, (int) field.getY() + m, width, height,"assets/g3.gif");
    Chip chip4 = new Chip((int) field.getX() + m + (width + m) * 3, (int) field.getY() + m, width, height,"assets/g4.gif");
    Chip chip5 = new Chip((int) field.getX() + m ,(int) field.getY() + m + (height + m), width, height,"assets/g5.gif");
    Chip chip6 = new Chip((int) field.getX() + m + (width + m),(int) field.getY() + m + (height + m), width, height,"assets/g6.gif");
    Chip chip7 = new Chip((int) field.getX() + m + (width + m) * 2,(int) field.getY() + m + (height + m),width, height,"assets/g7.gif");
    Chip chip8 = new Chip((int) field.getX() + m + (width + m) * 3,(int) field.getY() + m + (height + m),width, height,"assets/g8.gif");
    Chip chip9 = new Chip((int) field.getX() + m,(int) field.getY()+m+(height + m) * 2,width,height,"assets/g9.gif");
    Chip chip10 = new Chip((int) field.getX() + m + (width + m),(int) field.getY() + m + (height + m) * 2, width, height,"assets/g10.gif");
    Chip chip11 = new Chip((int) field.getX() + m + (width + m) * 2,(int) field.getY()+ m + (height + m) * 2, width, height,"assets/g11.gif");
    Chip chip12 = new Chip((int) field.getX() + m + (width + m) * 3,(int) field.getY() + m + (height + m) * 2,width, height,"assets/g12.gif");
    Chip chip13 = new Chip((int) field.getX() + m,(int) field.getY()+m+(height + m) * 3, width, height,"assets/g13.gif");
    Chip chip14 = new Chip((int) field.getX() + m + (width + m),(int) field.getY() + m + (height + m) * 3, width, height,"assets/g14.gif");
    Chip chip15 = new Chip((int) field.getX() + m + (width + m) * 2,(int) field.getY() + m + (height + m) * 3, width, height,"assets/g15.gif");


    Timer mainTimer = new Timer(25,this);// Таймер - задает интервал обновления всех событий
    // координаты мышки
    public static int mouseX;
    public static int mouseY;

    public enum STATES{ MENU, PLAY } //обьявляем перечсления
    public static STATES state = STATES.MENU;// переменная игры изначально - меню

    private final BufferedImage image;
    private final Graphics2D g;

    public Panel() {
        super(); // активируем консруктор родителя
        setFocusable(true); //передаем фокус
        requestFocus(); // акивируем

        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);// создаём объект буфера для хранения картинок
        g = (Graphics2D) image.getGraphics();//объекту присвоим элемент из буфера - картинку Graphics2D, применив метод getGraphics()

        list.add(chip1);list.add(chip2);list.add(chip3);list.add(chip4);list.add(chip5);list.add(chip6);list.add(chip7);
        list.add(chip8);list.add(chip9);list.add(chip10);list.add(chip11);list.add(chip12);list.add(chip13);list.add(chip14);list.add(chip15);
// заполняем список победных координат
        for (int i = 0; i < 15; i++) {// каждого объекта из списка
            Chip e = list.get(i); // выделяем элемент списка
            compare_x[i] =(int) e.getX();// получаем коорд элемента
            compare_y[i] =(int) e.getY();
        }

        addMouseListener( new Listeners());// добавляем обработчик событий клик мышь
        addKeyListener( new Listeners());// добавляем обработчик событий клавиатура
        addMouseMotionListener(new Listeners());//добавляем обработчик событий перемещение мышь

        field_x = field.getX();
        field_y = field.getY();
    }

    public void actionPerformed(ActionEvent e){//  все события игры
        if (state.equals(STATES.MENU)){// если мы сейчас в меню

            menu.drawBackgroundOfMenu(g);
            button_menu.drawButtonInMenu(g);
            button_exit.drawButtonInMenu(g);
            button_menu.clickOnButtonInMenu();
            button_exit.clickOnButtonInMenu();
            drawingOnPanel();// отрисовать все на панели
        }

        if (state.equals(STATES.PLAY)) { // если мы в процессе игры
            isVictory();// Сравнеие текущей комбинации с победной
            menu.drawBackgroundOfMenu(g);// отобразить фон меню
            field.drawGrid(g);//отрисовка игрового поля
            button_shuffle.drawButtonOfShuffle(g);// кнопка запутать
            button_shuffle.clickOnShuffleButton();// действие запутать
            //запутать комбинацию
            if(step < 50 && mixer && timerReach()){
                timerReach();
            }
            else if (step == 50){
                mixer = false;
                step = 0;
            }

            clickOnChip(); //перемещение по клику на фишке

            for (Chip chip : list) { //отрисовка фишек списком
                chip.drawChip(g);
            }
            drawingOnPanel(); // отрисовать все на панели
        }
    }
    // движение пятнашки по координатам
    public void clickOnChip() {
        double a, b;// переменные для координат возможного перемещения
        if (Listeners.clickChip){
            // выделяем элемент списка
            for (Chip e : list) {
                double ex = e.getX();// получаем коорд элемента
                double ey = e.getY();
                double ew = e.getW();
                double eh = e.getH();
                // на какой фишке курсор мышки
                if (Panel.mouseX > ex && Panel.mouseX < ex + ew && Panel.mouseY > ey && Panel.mouseY < ey + eh) {
                    // разрешение двигаться вверх
                    if (Panel.field_y + m < ey) {
                        a = ex;
                        b = ey - (eh + m);// возможные будущие координаты кликнутой кнопки
                        IsFreeChip(a, b); // проверка на свободную пятнашку
                        if (!search) {// совпадения координат с другими объектами нет
                            e.setY(b);// новые координаты фишки
                            e.setY(b);
                            return;
                        }
                    }

                    // разрешение двигаться вправо
                    if (Panel.field_x + m + (width + m) * 3 > ex) {
                        a = ex + (ew + m);
                        b = ey;// возможные будущие координаты кликнутой кнопки
                        IsFreeChip(a, b);
                        if (!search) {// совпадения координат с другими объектами нет
                            e.setX(a);// новые координаты фишки
                            e.setY(b);
                            return;
                        }
                    }
                    //разрешение двигаться вниз
                    if (Panel.field_y + m + (height + m) * 3 > ey) {
                        a = ex;
                        b = ey + (eh + m);// возможные будущие координаты кликнутой кнопки
                        IsFreeChip(a, b);
                        if (!search) {// совпадения координат с другими объектами нет
                            e.setY(b);// новые координаты фишки
                            e.setY(b);
                            return;
                        }
                    }
                    // разрешение двигаться влево
                    if (Panel.field_x + m < ex) {
                        a = ex - (ew + m);
                        b = ey;// возможные будущие координаты кликнутой кнопки
                        IsFreeChip(a, b);
                        if (!search) {// совпадения координат с другими объектами нет
                            e.setX(a);// новые координаты фишки
                            e.setY(b);
                            return;
                        }
                    }
                }
            }
        }
    }

    // Проверка свободной клетки для фишки
    private void IsFreeChip(double a, double b) {
        search = false; // предпологаем что перемещение разрешено
        // выделяем элемент списка
        for (Chip e : list) {// для каждого элемента из списка
            double ex = e.getX();// получаем коорд элемента
            double ey = e.getY();
            //Есть ли фишка с такими же координатами
            if (ex == a && ey == b) {
                search = true;
                return;
            }
        }
    }

    // Перемешать фишки
    private void shuffle() {
        double a, b;// переменные для координат возможного перемещения
        int chip = (int) ( Math.random() * 15);//случайный выбор номера фишки
        Chip e = list.get(chip); // выделяем элемент списка
        double ex = e.getX();// получаем коорд элемента
        double ey = e.getY();
        double ew = e.getW();
        double eh = e.getH();
        // разрешение двигаться вверх
        if (Panel.field_y + m < ey )  {
            a = ex;
            b = ey - (eh + m);// возможные будущие координаты кликнутой кнопки
            IsFreeChip(a,b);
            if(!search){// совпадения координат с другими объектами нет
                e.setY(b);// новые координаты фишки
                e.setY(b);
                step = step + 1;// одно перемещение фишки при перемешивании
                return;
            }
        }
        // разрешение двигаться вправо
        if (Panel.field_x + m + (width + m) * 3 > ex )  {
            a = ex + (ew + m);
            b = ey;
            IsFreeChip(a,b);
            if(!search){
                e.setX(a);
                e.setY(b);
                step = step + 1;
                return;
            }
        }
        //разрешение двигаться вниз
        if (Panel.field_y + m + (height + m) * 3 > ey )  {
            a=ex;
            b=ey + (eh + m);
            IsFreeChip(a,b);
            if(!search){
                e.setY(b);
                e.setY(b);
                step = step + 1;
                return;
            }
        }
        // разрешение двигаться влево
        if (Panel.field_x + m < ex )  {
            a = ex - (ew + m);
            b = ey;
            IsFreeChip(a,b);
            if(!search){
                e.setX(a);
                e.setY(b);
                step = step + 1;
                return;
            }
        }
        shuffle();//рекурсивно вызываем сами себя, пока количествое шагов не будет больше 50
    }
    // перемешивание в Timer
    Timer timer1 = new Timer(100, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            shuffle();//код который нужно выполнить в каждую единицу времени
            timer1.stop();
        }
    });

    // задержка перезапуска на время timer_d перемешивания в Timer
    public boolean timerReach(){
        if (timer_start == 0) { //если таймер не запущен
            timer_start = System.currentTimeMillis();// получаем текущее время милсек
            // (количество миллисекунд) длительность таймера
            long timer_delay = 100;
            timer_end = timer_start + timer_delay; // конечное время
            timer1.start();// запуск таймера
        }

        if (timer_end <= System.currentTimeMillis()) {
            timer_start = 0;// обнуляем счетчик
            return true;
        }
        else return false;
    }

    // Нахождение победной комбинации

    private void isVictory() {
        if(step >= 49){
            for (int i = 14; i > -1 ; i--) {
                Chip e = list.get(i);
                double ex = e.getX();// получаем коорд элемента
                double ey = e.getY();
                if (ex != compare_x[i] || ey != compare_y[i]){
                    return;
                }
            }
            JOptionPane.showMessageDialog(null, "ВЫ ВЫИГРАЛИ! Игра окончена!!");
            System.exit(1);
        }
    }
    //перенос изображения на панель
    private void drawingOnPanel() {
        Graphics g2 = this.getGraphics();// переоппред Graphics2d на Graphics
        g2.drawImage(image, 0, 0, null);// рисуем
        g2.dispose();// команда для уборщщика мусора
    }
}
