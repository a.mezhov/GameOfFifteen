package com.sanchos;

import java.awt.event.*;

public class Listeners implements MouseListener, KeyListener, MouseMotionListener {
    public static boolean clickMenu = false;// флаг клика ЛКМ в меню
    public static boolean clickChip = false;//флаг клика на пятнашке

    // проверка нажатой клавиши
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();// получить код нажатой клавиши
        if (key == KeyEvent.VK_ESCAPE) {
            if (Panel.state == Panel.STATES.PLAY)
                Panel.state = Panel.STATES.MENU; // переход в меню из игры
        }
    }
    // проверка отжатой клавиши
    public void keyReleased(KeyEvent e){
    }
    public void keyTyped(KeyEvent e){
    }
    @Override
    public void mouseClicked(MouseEvent e) {
    }
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (Panel.state == Panel.STATES.MENU) {
                clickMenu = true;// нажатие ЛКМ  в меню
            }
            if (Panel.state == Panel.STATES.PLAY) {
                clickChip = true;// нажатие ЛКМ в меню
            }
        }

    }

    public void mouseReleased(MouseEvent e){
        if (e.getButton() == MouseEvent.BUTTON1) {

            if (Panel.state == Panel.STATES.MENU) {
                clickMenu = false;// отпуск ЛКМ в меню
            }
            if (Panel.state == Panel.STATES.PLAY) {
                clickChip = false;// нажатие ЛКМ в меню
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {
        Panel.mouseX = e.getX();// получить координату х при перемещении мышки
        Panel.mouseY = e.getY();// получить координату у при перемещении мышки
    }
}


